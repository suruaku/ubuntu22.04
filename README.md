## About The Project

Setup Ubuntu 22.04, [Traefik](https://doc.traefik.io/traefik/) and [Portainer](https://docs.portainer.io/) in Docker with [Ansible](https://www.digitalocean.com/community/cheatsheets/how-to-use-ansible-cheat-sheet-guide).

Running this playbook will perform the following actions on your Ansible hosts:

1. Install aptitude, which is preferred by Ansible as an alternative to the apt package manager.
2. Create a new sudo user and set up passwordless sudo.
3. Install system packages .
4. Copy a local SSH public key and include it in the authorized_keys file for the new administrative user on the remote host.
5. Install modern and secure [sshd config](files/10-sshd.conf).
6. Configure the UFW firewall to only allow SSH connections and deny any other requests.
7. Configure and enable fail2ban.
8. Minimal working vimrc.
9. Start Portainer and Traefik in docker.

### Prerequisites

* Remove `.example` from file names and update values.

* Update, if needed, [path](tasks/user.yml) for your ssh public key.

* Create CNAME record for portainer admin panel in your DNS provider.

* Update your email and domain for portainer admin panel in [docker-compose.yml](files/docker-compose.yml). 

* Install Ansible

### Usage

1. Clone the repo
   ```sh
   git clone https://gitlab.com/suruaku/ubuntu22.04.git
   ```
2. Run
    ```sh
    ansible-playbook ./main.yml -u root -k
    ```

The `-u` flag specifies which user to log into on the remote server.
Since you’ve yet to setup your remote server, root is your only option.
The `-k` flag allows you to enter your SSH password.

3. Access Portainer admin dashboard.

Some parts of this guide are from [DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-use-ansible-to-automate-initial-server-setup-on-ubuntu-22-04).
